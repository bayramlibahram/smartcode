const keys = require('../keys/config');

module.exports = function(email){
    return {
        to:email,
        from:keys.EMAIL_FROM,
        subject:'Smartcode qeydiyyat',
        html:`
        <h1>Qeydiyyat uğurla başa catmışdır</h1>
        <a href="${keys.BASE_URL}/auth/login">Smart panel</a>
        <hr>
        <a href="${keys.BASE_URL}">Smartcode.az</a>
        `
    }
}
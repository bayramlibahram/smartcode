const keys = require('../keys/config');

module.exports = function(email, token){
    return {
        to:email,
        from:keys.EMAIL_FROM,
        subject:'Reset password',
        html:`
        <h1>Reset password</h1>
        <p>Ignore this email if you not forget the password</p>
        <p>Or click the link in down below</p>
        <a href="${keys.BASE_URL}/auth/password/${token}">Recover password</a>
        <hr>
        <a href="${keys.BASE_URL}">Shop web site</a>
        `
    }
}
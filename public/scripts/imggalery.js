﻿
function ImgGalery() {
    document.addEventListener('DOMContentLoaded', function () {
        const imgItem = document.querySelectorAll(".m-img");

        for (var i = 0; i < imgItem.length; i++) {
            imgItem[i].addEventListener('click', item => {
                const mainImage         = document.getElementById('adminLayout_mainimage');
                const mainImgHidden     = document.getElementById('adminLayout_mainimage');
                const mainimg_fileinput = document.getElementById('adminLayout_mainimg_hidded');
                mainImage.src           = item.target.src;
                mainImgHidden.value     = item.target.src;
                mainimg_fileinput.value = null;
            });
        }
    });
}

ImgGalery();

//On UpdatePanel Refresh
let prm = Sys.WebForms.PageRequestManager.getInstance();
if (prm !== null) {
    prm.add_endRequest(function (sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            ImgGalery();
        }
    });
};
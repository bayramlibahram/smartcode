    "use strict"
    document.addEventListener("DOMContentLoaded", () => {

        const toDate        = date =>{
            return new Intl.DateTimeFormat('az-AZ',{
                day:'2-digit',
                month:'long',
                year:'numeric',
                hour:'2-digit',
                minute:'2-digit',
                second:'2-digit'
            }).format(new Date(date));
        }
        document.querySelectorAll('.sc-date').forEach(node=>{
            node.textContent = toDate(node.textContent);
        });

        class ConvertDate {
           constructor(hiddenDateId, datePickerId) {
               this._hiddenDate = hiddenDateId
               this._datepicker = datePickerId
           }

           convertToAZE(){
               const hiddenDate = document.getElementById(this._hiddenDate)
               const datePicker = document.getElementById(this._datepicker)
               if(hiddenDate !== null && datePicker !== null){

                   const dbDate             = new Date(hiddenDate.value)
                   const splitDate          = dbDate.toLocaleDateString().split('/')
                   const appDate            = `${splitDate[1]}/${splitDate[0]}/${splitDate[2]}`
                   const hoursAndMinutes    = `${dbDate.getHours()}:${dbDate.getMinutes()}`
                   const lastDate           = `${appDate} ${hoursAndMinutes}`

                   //set last converted date to appDate input
                   datePicker.value = lastDate
               }
           }
       }

        //mm//dd//yyyy
        //dd//mm//yyyy


        const appDate = new ConvertDate('hiddendate','datepciker')
        appDate.convertToAZE()

        CKEDITOR.replaceAll();

        //Jquery bootstrap datetimepicker configuration
        $(function () {
            $('.datetimepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm',
                icons: {
                    time: "fa fa-clock",
                    date: "fa fa-calendar-day",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-remove'
                }
            });
        });


        const manage_img = document.getElementById('post_img')
        if(manage_img !== null){
            viewImage("post_img", "post_file", "custom_file", "file_btn");
        }




    })



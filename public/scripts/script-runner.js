document.addEventListener("DOMContentLoaded", function () {

/*  particlesJS.load(
    "particles-js",
    "./scripts/particlesjs-config.json",
    function () {
      // console.log("callback - particles.js config loaded");
    }
  );*/


  const wordsArray      = ["CODE", "ERP", "OFFICE", "TASK", "SOLUTION"];
  const runner          = document.getElementById("runnertxt");
  let index             = 0;
  const wordsCount      = wordsArray.length;
  let wordsCountIndex   = 0;
  let trigger           = false;
  const speed           = 200;

  function typeEffect() {
    if (wordsCountIndex < wordsCount) {
      if (trigger == false) {
        if (index < wordsArray[wordsCountIndex].length) {
          runner.innerHTML += wordsArray[wordsCountIndex].charAt(index);
          index++;
          setTimeout(typeEffect, speed);
        } else {
          trigger = true;
          if (index > 0) {
            runner.innerHTML = runner.innerHTML.slice(0, -1);
            index--;

            setTimeout(typeEffect, speed);
          }
        }
      } else {
        if (index > 0) {
          runner.innerHTML = runner.innerHTML.slice(0, -1);
          index--;

          setTimeout(typeEffect, speed);
        } else {
          wordsCountIndex++;
          trigger = false;
          setTimeout(typeEffect, speed);
        }
      }
    }
    else {
      wordsCountIndex = 0;
      setTimeout(typeEffect, speed);
    }
  }

  typeEffect();

  // AOS.init();
});

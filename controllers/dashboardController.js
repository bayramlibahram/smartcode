class DashboardController{
    async getDashboard(req, res){
        res.render('manage/dashboard',{
            title: 'Dashboard',
            layout:'manage',
        })
    }
}

module.exports = new DashboardController()
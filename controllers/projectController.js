const path = require("path");
const Project = require("../models/project");
const ConvertDate = require("../helpers/date");

class ProjectController {
  async getProjects(req, res) {
    const projects = await Project.find({}).sort({ date: -1 }).lean();

    res.render("manage/projects", {
      title: "Projects",
      layout: "manage",
      projects,
    });
  }

  async newProject(req, res) {
    res.render("manage/project", {
      title: "New Project",
      layout: "manage",
      isNew: true,
      projectAction: "/manage/project/add",
      date: new Date().toUTCString(),
    });
  }

  async getProject(req, res) {
    if (!req.query.allow) {
      return res.redirect("/manage/projects");
    }
    const project = await Project.findById(req.params.id).lean();

    res.render("manage/project", {
      title: "New Project",
      layout: "manage",
      isNew: false,
      projectAction: "/manage/project/edit",
      project,
    });
  }

  async editProject(req, res) {
    try {
      const convertDate = new ConvertDate(req.body.date);
      const id = req.body.id;
      if (!req.files || Object.keys(req.files).length === 0) {
        await Project.findByIdAndUpdate(
          { _id: id },
          {
            $set: {
              title: req.body.title,
              url: req.body.url,
              img: req.body.hiddenimg,
              date: convertDate.convertToUTC(),
            },
          },
          { new: true }
        );
        res.redirect("/manage/projects");
      } else {
        // Accessing the file by the <input> File name="post_file"
        let targetFile = req.files.post_file;

        //mv(path, CB function(err))
        await targetFile.mv(
          path.join(
            __dirname,
            "../",
            "public",
            "images",
            "projects",
            targetFile.name
          ),
          async (err) => {
            if (err) {
              console.log(err);
              return res.status(500).send(err);
            } else {
              const id = req.body.id;

              await Project.findOneAndUpdate(id, {
                title: req.body.title,
                url: req.body.url,
                img: targetFile.name,
                date: convertDate.convertToUTC(),
              });
              res.status(200).redirect("/manage/projects");
            }
          }
        );
      }
    } catch (e) {
      console.log(e);
    }
  }

  async addProject(req, res) {
    try {
      if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send("No files were uploaded.");
      }

      // Accessing the file by the <input> File name="post_file"
      let targetFile = req.files.post_file;

      //mv(path, CB function(err))
      await targetFile.mv(
        path.join(
          __dirname,
          "../",
          "public",
          "images",
          "projects",
          targetFile.name
        ),
        (err) => {
          if (err) {
            console.log(err);
            return res.status(500).send(err);
          } else {
            const convertDate = new ConvertDate(req.body.date);
            const project = new Project({
              title: req.body.title,
              url: req.body.url,
              img: targetFile.name,
              date: convertDate.convertToUTC(),
            });
            project.save();
            res.redirect("/manage/projects");
          }
        }
      );
    } catch (e) {
      console.log(e);
    }
  }

  async removeProject(req, res) {
    try {
      await Project.deleteOne({
        _id: req.body.id,
      });
      res.status(200).redirect("/manage/projects");
    } catch (err) {
      console.log(err);
    }
  }
}

module.exports = new ProjectController();

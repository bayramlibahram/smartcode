const Appeal = require('../models/appeals')

class ContactusController {
    async getContactUs(req, res) {
        res.render('contactus',{
            title: 'Əlaqə',
            isContactUs: true,
        })
    }

    async appeal(req, res) {
        const{name_surname, email, message} = req.body
        const appeal = new Appeal({
            name_surname,
            email,
            message

        })
        await  appeal.save().then(()=>{
            res.redirect('/')
        })
    }
}

module.exports = new ContactusController();
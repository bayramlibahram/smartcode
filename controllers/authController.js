const User = require('../models/user')
const bcrypt = require("bcryptjs");
const crypto = require('crypto')
//add to nodemailer package
const nodemailer = require('nodemailer')
//use the keys config
const keys = require('../keys/config')
//use for send registration message
const regEmail = require('../email/registration')// return the function
const resetEmail = require('../email/reset')
const gmailConfiguration = require('../email/gmailconf')
const mailTransporter = nodemailer.createTransport(gmailConfiguration);
const {validationResult} = require('express-validator')


class AuthController {

    async getLogin(req, res) {
        try {
            res.render('auth/login', {
                title: 'Login',
                loginError: req.session.flash !== undefined ? req.flash('loginError') : null
            })
            req.session = null
        } catch (e) {
            console.log(e)
        }
    }

    async getRegister(req, res) {
        res.render('auth/register', {
            title: 'Register',
            registerError: req.session.flash !== undefined ? req.flash('registerError') : null
        })
        req.session = null
    }

    async loginUser(req, res) {
        try {
            const error = validationResult(req)

            if (!error.isEmpty()) {
                req.flash('loginError', error.array()[0].msg)
                return res.status(422).redirect('/auth/login')
            }

            const {email, password} = req.body

            const user = await User.findOne({email}).select('login password')

            if (!user) {
                req.flash('loginError', 'Email ve yaxud şifrə yalnışdır! ')
                return res.redirect('/auth/login')
            }

            const comparedPassword = await bcrypt.compare(password, user.password)

            if (!comparedPassword) {
                req.flash('loginError', 'Email ve yaxud şifrə yalnışdır! ')
                return res.redirect('/auth/login')
            }

            req.session.user = user;
            req.session.isAuthenticated = true
            req.session.save(err => {
                if (err) {
                    throw err
                    return res.status(403).redirect('/auth/login')
                }
                res.redirect('/manage/dashboard')
            })

        }
        catch (e) {
            console.log(e)
        }
    }

    async registerUser(req, res) {
        try {
            const {name, surname, email, password} = req.body
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                req.flash('registerError', errors.array()[0].msg)
                return res.status(422).redirect('/auth/register')
            }
            const hashPassword = await bcrypt.hash(password, 10)
            if (hashPassword) {
                const user = new User({
                    name,
                    surname,
                    email,
                    password: hashPassword
                })
                await user.save().then(() => {
                    res.redirect('/auth/login')
                })
                await mailTransporter.sendMail(regEmail(email))
            }
        } catch (e) {
            console.log(e)
        }
    }

    async logout(req, res) {
        req.session = null
        res.clearCookie('csrfSecret');
        res.redirect('/auth/login')
    }

    async getResetPasswords(req, res) {
        res.render('auth/reset', {
            title: 'Reset password'
            // resetError: req.session.flash !== undefined ? req.flash('resetErroe') : null,
        })
    }

    createAndSendToken(req, res) {

        try {
            //Generate token
            crypto.randomBytes(32, async (err, buffer) => {
                if (err) {
                    return res.redirect('/auth/reset')
                } else {
                    const token = buffer.toString('hex')
                    const candidate = await User.findOne({email: req.body.email})
                    if (candidate) {
                        candidate.resetToken = token
                        candidate.resetTokenExp = Date.now() + 60 * 60 * 1000
                        await candidate.save()
                        await mailTransporter.sendMail(resetEmail(candidate.email, token), (err, data) => {
                            if (err) {
                                console.error(err)
                                return res.redirect('/auth/reset')
                            } else {
                                console.log(data)
                                return res.redirect('/auth/login')
                            }
                        })
                    } else {
                        console.log('This email is not exist')
                        return res.redirect('/auth/reset')
                    }
                }
            })
        } catch (e) {
            console.log(e)
        }

    }

    async getResetToken(req, res) {
        if (!req.params.token) {
            return res.redirect('/auth/login')
        } else {
            const user = await User.findOne({
                resetToken: req.params.token,
                resetTokenExp: {$gt: Date.now()}
            })
            if (user) {
                res.render('auth/password', {
                    title: 'Reset Password',
                    userId: user._id.toString(),
                    userToken: user.resetToken.toString(),

                })
            } else {
                return res.redirect('/auth/login')
            }
        }
    }

    async resetPassword(req, res) {
        try {
            const {userId, userToken, password} = req.body

            const user = await User.findOne({
                _id: userId,
                resetToken: userToken,
                resetTokenExp: {$gt: Date.now()}
            })

            if (user) {
                user.password = await bcrypt.hash(password, 10)
                user.resetToken = undefined
                user.resetTokenExp = undefined
                await user.save().then(() => {
                    console.log('user password is changed')
                    return res.redirect('/auth/login')
                })
            } else {
                console.log('user password is not changed')
                return res.redirect('/auth/login')
            }

        } catch (e) {
            console.log(e)
        }
    }
}

module.exports = new AuthController()
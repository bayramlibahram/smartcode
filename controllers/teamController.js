const Team = require('../models/team')

class TeamController {
    async getTeams(req, res) {
         const teams = await Team.find({}).lean();
         res.render('teams',{
             title: 'Komandamız',
             isTeam: true,
             teams
         })

    }
}

module.exports = new TeamController()
const path = require("path")
const Post = require('../models/post')
const ConvertDate = require('../helpers/date')

class PostController {
    async getPost(req, res) {

        const posts = await Post.find({}).sort({date: -1}).lean();

        res.render('manage/posts', {
            title: 'Posts',
            layout: 'manage',
            posts
        })
    }

    async newPost(req, res) {
        res.header('Content-Security-Policy', 'img-src * \'self\' data: https:; default-src *; style-src \'self\' \'unsafe-inline\'; script-src \'self\' \'unsafe-inline\' ');
        res.render('manage/post', {
            title: 'New post',
            layout: 'manage',
            isNew: true,
            projectAction: '/manage/post/add',
            date: new Date().toUTCString()
        })
    }

    async addPost(req, res) {

        try {
            if (!req.files || Object.keys(req.files).length === 0) {
                return res.status(400).send('No files were uploaded.');
            }

            let targetFile = req.files.post_file;

            await targetFile.mv(
                path.join(__dirname, "../", 'public', "images", "posts", targetFile.name),
                async (err)=>{
                    if(err){
                        throw  err
                        console.error(err)
                    }
                    else {
                        const convertDate = new ConvertDate(req.body.date)

                        const post = new Post({
                            post_title_az:req.body.post_title_az,
                            post_title_en:req.body.post_title_en,
                            post_seo_az:req.body.post_seo_az,
                            post_seo_en:req.body.post_seo_en,
                            post_detail_az:req.body.post_detail_az,
                            post_detail_en:req.body.post_detail_en,
                            post_view_az:req.body.post_view_az.toString() === 'true' ? true : false,
                            post_view_en:req.body.post_view_en.toString() === 'true' ? true : false,
                            post_img:targetFile.name,
                            date: convertDate.convertToUTC()
                        })

                        await post.save();
                        res.redirect('/manage/posts')
                    }
                }
            )

        }
        catch (e) {
            console.log(e);
        }
    }
}

module.exports = new PostController()
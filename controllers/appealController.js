const Appeal = require('../models/appeals')
class AppealController {
    async getAppeals(req, res){
        try {
            const appeals = await Appeal.find({}).lean()
            console.log(appeals)
            res.render('manage/appeals', {
                title: 'Appeals',
                layout: 'manage',
                appeals
            })
        }
        catch(err){
            console.log(err)
        }
    }
}

module.exports = new AppealController()
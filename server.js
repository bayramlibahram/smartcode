const {
    path,
    express,
    expressHandlebars,
    helmet,
    mongoose,
    MongoStore,
    csrf,
    bodyParser,
    cookieParser,
    fileUpload,
    flash,
    compression,
    config,
    session
} = require("./modules/index");

const varMiddleware = require("./middlewares/variables");
const userMiddleware = require("./middlewares/user");
const errorHandler = require("./middlewares/errors");

const keys = require("./keys/config");
const csrfProtection = csrf({cookie: true});
const app = express();

const hbs = expressHandlebars.create({
    defaultLayout: "main",
    extname: "hbs",
});
const store = new MongoStore({
    collection: "session",
    uri: keys.DB_URL,
});

app.engine("hbs", hbs.engine);
app.set("view engine", "hbs");
app.set("views", "views");
app.disable("etag");

app.use(
    compression({
        level: 6,
        threshold: 100 * 1000,
    })
);

app.use(helmet({
    noSniff: false,
    contentSecurityPolicy: {
        directives: {
            ...helmet.contentSecurityPolicy.getDefaultDirectives(),
            "img-src": ["'self'", "https://smartcode.az"],
            "img-src": ["'self'", "data:"],
        },
    },
}));

app.use(
    fileUpload({
        useTempFiles: false,
        tempFileDir: path.join(__dirname, "tmp"),
    })
);

app.use(express.static(path.join(__dirname, "public")));
app.use("/manage/", express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());
app.use(session({
    secret: keys.SESSION_KEY,
    resave: false,
    saveUninitialized: false,
    store: store,
    unset: "destroy",
}));

app.use(csrfProtection);
app.use(flash());
app.use(varMiddleware);
app.use(userMiddleware);

app.use("/", require("./routes/home.js"));
app.use("/projects", require("./routes/projects"));
app.use("/services", require("./routes/services"));
app.use("/contactus", require("./routes/contactus"));
app.use("/teams", require("./routes/teams"));
app.use("/auth", require("./routes/auth"));
app.use("/manage", require("./routes/manage/dashboard"));
app.use("/manage", require("./routes/manage/projects"));
app.use("/manage", require("./routes/manage/posts"));
app.use("/manage", require("./routes/manage/appeals"));

app.use(errorHandler);

const PORT = config.get("PORT");

const start = async () => {
    try {
        await mongoose
            .connect(config.get("MONGO_URL"))
            .then(() => {
                console.log("DB IS CONNECTED");
            })
            .catch((err) => {
                console.log(err);
            });
        app.listen(PORT, () => {
            console.log(`Server has been started on port ${PORT}`);
        });
    } catch (e) {
        console.log(e);
    }
};

start();

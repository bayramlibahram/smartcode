const { Router }        = require('express')
const router            = Router()
const teamController    = require('../controllers/teamController')

router.get('/', teamController.getTeams)

module.exports  = router
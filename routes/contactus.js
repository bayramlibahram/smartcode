const {Router} = require('express')
const router = Router()
const {getContactUs, appeal} = require('../controllers/contactusController')


router.get('/', getContactUs)

router.post('/appeal', appeal)

module.exports = router
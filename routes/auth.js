const {Router}              = require('express')
const authController        = require('../controllers/authController')
const router                = Router()
const{registerValidators, loginValidators}   = require('../utils/validators')

router.get('/login', authController.getLogin)

router.get('/register', authController.getRegister)

router.post('/register', registerValidators, authController.registerUser)

router.post('/login', loginValidators, authController.loginUser)

router.get('/logout', authController.logout)

router.get('/reset' , authController.getResetPasswords)

router.post('/reset', authController.createAndSendToken)

router.get('/password/:token', authController.getResetToken)

router.post('/password', authController.resetPassword)

module.exports = router
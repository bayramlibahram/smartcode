const {Router} = require('express');
const router = Router();
const Project = require('../models/project');


router.get('/', async (req, res) => {
    const projects = await Project.find({}).sort({date: -1}).lean();
    res.statusCode = 200;
    res.render("home", {
        title: "SMARTCODE.AZ",
        isHome: true,
        projects
    });
});
module.exports = router;
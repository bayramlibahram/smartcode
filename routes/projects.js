const {Router} = require('express');
const router = Router();
const Project = require('../models/project');

router.get('/', async (req, res) => {
    const projects = await Project.find({}).lean();
    res.render('projects', {
        title: "Lahiyələr",
        isProject: true,
        projects
    })
});

module.exports = router;
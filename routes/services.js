const {Router}              = require('express')
const serviceController     = require('../controllers/servicesController')
const router                = Router()



router.get('/', serviceController.getServices)

module.exports = router



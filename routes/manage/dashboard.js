const {Router}              = require('express')
const auth                  = require('../../middlewares/auth')
const dashboardController   = require('../../controllers/dashboardController')
const router                = Router();

router.get('/dashboard', auth, dashboardController.getDashboard)

module.exports = router;
const { Router }        = require('express')
const auth              = require('../../middlewares/auth')
const authController    = require('../../controllers/appealController')
const router            = Router()

router.get('/appeals', auth, authController.getAppeals)


module.exports = router
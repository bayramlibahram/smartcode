const {Router}                  = require('express')
const projectController         = require('../../controllers/projectController')
const auth                      = require('../../middlewares/auth')
const router                    = Router()

router.get('/projects', auth, projectController.getProjects)

router.get('/project', auth, projectController.newProject)

router.get('/project/:id/edit', auth, projectController.getProject)

router.post('/project/edit', auth, projectController.editProject)

router.post('/project/add', auth, projectController.addProject)

router.post('/project/remove', auth, projectController.removeProject)

module.exports = router
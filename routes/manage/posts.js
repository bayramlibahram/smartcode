const {Router}                  = require('express')
const postController            = require('../../controllers/postController')
const auth                      = require('../../middlewares/auth')
const router                    = Router()

router.get('/posts', auth,  postController.getPost )

router.get('/post', auth, postController.newPost )

router.post('/post/add', auth, postController.addPost)

module.exports = router
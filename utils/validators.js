const {body} = require('express-validator');
const User = require('../models/user');

exports.registerValidators = [
    body('email').isEmail().withMessage('not valid email')
        .custom(async (value, {req}) => {
            try {
                const user = await User.findOne({email: value})
                if (user) {
                    return Promise.reject('This email is already registered')
                }
            } catch (e) {
                console.log(e)
            }
        })
        .normalizeEmail()
        .trim(),
    body('password', 'Password must be numbers and letters').isLength({min: 5, max: 12}).trim(),
    body('name').isLength({min: 3}).withMessage('Name length must be greather than 3 symbol'),
    body('surname').isLength({min: 5}).withMessage('Surname length must be greather than 5 symbol')

]

exports.loginValidators = [
    body('email')
        .isEmail()
        .withMessage('Email düzgün qedy olunmayıb'),
    body('password', 'Parol simvol və rəqəmlərdən ibarət olmalıdır')
        .isLength({min: 5, max: 12}).trim(),
]

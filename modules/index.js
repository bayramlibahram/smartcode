const path = require("path");
const express = require("express");
const helmet = require("helmet");
const mongoose = require("mongoose");
const expressHandlebars = require("express-handlebars");
const session = require("express-session");
const MongoStore = require("connect-mongodb-session")(session);
const csrf = require("csurf");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const fileUpload = require("express-fileupload");
const flash = require("connect-flash");
const compression = require("compression");
const config = require("config");

module.exports = {
    path,
    express,
    expressHandlebars,
    helmet,
    mongoose,
    MongoStore,
    csrf,
    bodyParser,
    cookieParser,
    fileUpload,
    flash,
    compression,
    config,
    session
}
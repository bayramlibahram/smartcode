class ConvertDate {

    constructor(date){
        this._date = date
    }

    convertToUTC(){
        const fullDateArray     = this._date.split(' ')
        const dateArray         = fullDateArray[0].split('/')
        //dd/mm/yyyy
        const convertedDate     =`${dateArray[1]}/${dateArray[0]}/${dateArray[2]}`
        //dd/mm/yyyy hh:mm
        return     new Date(`${convertedDate} ${fullDateArray[1]}`).toUTCString()


    }
}

module.exports = ConvertDate
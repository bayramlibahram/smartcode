const {Schema, model} = require('mongoose')

const teamSchema = new Schema({
    name: {
        type:String,
        required: true,
        minLength:3,
        maxLength:10
    },
    name: {
        type:String,
        required: true,
        minLength:8,
        maxLength:15
    },
    birthday:{
        type:Date
    },
    position: {
        type:String,
        required: true,
    },
    skills: {
        type:Array
    },
    detail:{
        type:String
    }
})

module.exports = model('Team', teamSchema)
const {Schema, model} = require('mongoose')

const serviceSchema = new Schema({

    name:{
        type:String,
        required:true,
        minLength:5
    },

    icon:{
        type:String,
        required:true,
        minLength:5
    }

})

module.exports = model('Service',serviceSchema)
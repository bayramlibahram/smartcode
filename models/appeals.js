const{Schema, model} = require('mongoose')

const appealSchema = new Schema({
    name_surname:{
        type:String,
        required:true,
        maxLength:20
    },
    email:{
        type:String,
        required:true,
        maxLength:30
    },
    message:{
        type:String,
        required:true,
        maxLength:2000
    },
    date:{
        type:Date,
        default:new Date().toString()
    }
})

module.exports = model('Appeal', appealSchema)
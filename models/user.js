const{Schema, model} = require('mongoose')

const userSchema = new Schema({
    name:{
        type:String,
        required:true,
        minLength:3,
        maxLength:10
    },
    surname:{
        type:String,
        required:true,
        minLength:5,
        maxLength:15
    },
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true,
        minLength:5,
        maxLength:150,
    },
    resetToken:String,
    resetTokenExp:String
})

module.exports = model('User', userSchema)
const {Schema, model} = require('mongoose');

const projectSchema = new Schema({
      title:{
            type: String,
            required: true,
            minLength:3
      },
      url:{
            type: String,
            required: true,
            minLength:6
      },
      img:{
            type:String,
            required: true,
            maxLength:50
      },
      date:{
            type:Date
      }
});

module.exports = model('Project',projectSchema);
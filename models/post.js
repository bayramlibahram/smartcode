const{Schema, model} = require('mongoose')

const postSchema = new Schema({
    is_delete:{
        type: 'boolean',
        default:false
    },
    is_active:{
        type: 'boolean',
        default:true
    },
    post_title_az:{
        type: 'String',
        required:true,
        minlength:5,
    },
    post_title_en:{
        type: 'String',
        required:true,
        minlength:5,
    },
    post_seo_az:{
        type: 'String',
        required:true,
        minlength:5,
    },
    post_seo_en:{
        type: 'String',
        required:true,
        minlength:5,
    },
    post_detail_az:{
        type: 'String'
    },
    post_detail_en:{
        type: 'String'
    },
    post_view_az:{
        type: 'Boolean',
    },
    post_view_en:{
        type: 'Boolean',
    },
    post_img:{
        type: 'String',
        required:true,
        maxlength:150
    },
    date:Date
})

module.exports = model('Post',postSchema)
